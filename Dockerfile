FROM node:16-alpine3.15

# needed to run docker in docker on Amazon Linux 2 in CodeBuild projects
# See AWS Case: 9042843731 in account 0016
VOLUME /var/lib/docker

RUN mkdir /etc/docker && echo '{"experimental": true}' > /etc/docker/daemon.json

RUN apk add --update --no-cache \
      bash \
      docker \
      python3 \
      py3-pip \
      && \
    rm -rf /var/cache/apk/*


# Install CDK
RUN npm install -g aws-cdk@2.59.0
RUN python3 --version
RUN python3 -m pip install --no-cache-dir --upgrade pip && \
    python3 -m pip install --no-cache-dir \
      "aws-cdk-lib==2.59.0" \
      "constructs>=10.0.0,<11.0.0" \
      aws_cdk.aws_lambda_python_alpha \
      boto3 && \
    rm -rf /root/.cache/pip/*

